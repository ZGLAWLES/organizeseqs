import shutil
import os
from tkinter import Tk
from tkinter.filedialog import askdirectory
from tkinter.simpledialog import askstring
from tkinter.messagebox import askokcancel
from tkinter.messagebox import showerror
from pathlib import Path

while True:
    # Displays File Manager Tool to Select Folder
    Tk().withdraw()
    unorganized_path = askdirectory(title="Select Folder")
    if not unorganized_path:
        exit(0)

    # Confirm the correct folder was selected
    confirmed = askokcancel(title="Directory Confirmation",
                            message="Are you sure you want to proceed with the following directory?\n(" +
                                    unorganized_path + ")")
    if not confirmed:
        exit(0)
    elif confirmed:
        break

# Create Path Object
unorganized_path = Path(unorganized_path)

while True:
    # Get the name for the new folder from the user
    new_folder_name = askstring(title="New Folder Name", prompt="Please enter the name of the new directory: ")
    if new_folder_name is None:
        exit(0)
    elif new_folder_name.isalnum():
        break
    else:
        showerror(title="Folder Name Error", message="The folder name needs to be alphanumeric.")

# Generate the path for the new folder
organized_path = unorganized_path.parent.joinpath(new_folder_name)

# Create the new folder
try:
    os.mkdir(organized_path)
except OSError as error:
    print(error)
    print("Please try running the script again.")
    print("Exiting...")
    exit(0)

print("Copying Files...")

# Copy all fastq.gz files in the selected root directory to the new folder
for subdir, dirs, files in os.walk(unorganized_path):
    for file in files:
        if file[-8:] == "fastq.gz":
            src_path = os.path.join(subdir, file)
            dest_path = os.path.join(organized_path, file)
            shutil.copy(src_path, dest_path)
        else:
            print("WARNING: It appears there were non-fastq.gz files located in the selected directory.\n"
                  "         Force quit the program if the wrong root directory was chosen.")

print("Files copied successfully!")
print("Exiting...")
